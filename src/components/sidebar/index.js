import React from 'react';
import './style.css';
import {NavLink} from "react-router-dom";

class Sidebar extends React.Component {

    render() {

        return (
            <div className="sidebar">
                <NavLink className="sidebar_element " to="/" >Просмотр записей</NavLink>
                <NavLink className="sidebar_element" to="/add_record" >Добавить конфиг</NavLink>
                <NavLink className="sidebar_element" to="/add_student">Добавить ученика</NavLink>
            </div>
        )
    }

}



export default Sidebar;