// вовлеченность 1) кол-во положительных/негативным 2) активные к пассивным 3)
//График эмоций со временем
import React, { Component } from "react";
import Chart from "react-apexcharts";
import '../style.scss';
import Graph_links from "../../record/Graph_links";
import {Link} from "react-router-dom";
import Back from "../Back";

class Posneg extends Component {


    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        let link = "https://facerecog.miem.tv/record/" + this.props.match.params.room + "/" + this.props.match.params.date + "/" + this.props.match.params.time
        fetch(link)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data.frames
                });
            });
    }

    render() {
        function getMaxOfArray(numArray) {
            return Math.max.apply(null, numArray);
        }

        if (this.state.isLoaded) {
            let dataJson = {}
            dataJson.frames = this.state.items
            console.log(dataJson.frames)
            let framesCount = dataJson.frames.length;
            console.log(framesCount)
            let FrameEmotes = [];
            let Emotes = [[], [], [], [], [], []]
            let cnt = 0
            let maxNum = 0
            let frames_skipped = 25
            FrameEmotes.push([0, 0, 0, 0, 0, 0]);
            for (let i = 0; i < framesCount; i += 1) {
                if (i % frames_skipped == 0 && i != 0) {
                    Emotes[0].push(FrameEmotes[cnt][0] / frames_skipped)
                    Emotes[1].push(FrameEmotes[cnt][1] / frames_skipped)
                    Emotes[2].push(FrameEmotes[cnt][2] / frames_skipped)
                    Emotes[3].push(FrameEmotes[cnt][3] / frames_skipped)
                    Emotes[4].push(FrameEmotes[cnt][4] / frames_skipped)
                    Emotes[5].push(FrameEmotes[cnt][5] / frames_skipped)
                    FrameEmotes.push([0, 0, 0, 0, 0, 0]);
                    cnt++
                }
                for (let j = 0; j < dataJson.frames[i].length; j++) {

                    if (dataJson.frames[i] != undefined)
                        if (dataJson.frames[i][j][1] == "FEAR")
                            FrameEmotes[cnt][0]++;
                    if (dataJson.frames[i][j][1] == "NEUTRAL")
                        FrameEmotes[cnt][1]++;
                    if (dataJson.frames[i][j][1] == "HAPPY")
                        FrameEmotes[cnt][2]++;
                    if (dataJson.frames[i][j][1] == "SAD")
                        FrameEmotes[cnt][3]++;
                    if (dataJson.frames[i][j][1] == "ANGRY")
                        FrameEmotes[cnt][4]++;
                    if (dataJson.frames[i][j][1] == "SURPRISE")
                        FrameEmotes[cnt][5]++;
                }

            }
            let AllEmotes = []
            for (let i = 0; i < FrameEmotes.length; i++) {
                let emotes_posneg = (Emotes[2][i] + Emotes[5][i]) / (Emotes[0][i] + Emotes[1][i] + Emotes[2][i] + Emotes[3][i] + Emotes[4][i] + Emotes[5][i]);
                if(isNaN(emotes_posneg))
                    emotes_posneg = 0
                AllEmotes.push(emotes_posneg)
            }
            let graph = {

                series: [
                    {
                        name: "Positive/negative",
                        data: AllEmotes
                    }
                ],
                options: {
                    chart: {
                        height: 300,
                        type: 'line',
                        dropShadow: {
                            enabled: true,
                            color: '#000',
                            top: 18,
                            left: 7,
                            blur: 10,
                            opacity: 0.2
                        },
                        toolbar: {
                            autoSelected: 'zoom'
                        }
                    },
                    colors: ['#00FF00', '#FF0000'],
                    dataLabels: {
                        enabled: true,
                    },
                    stroke: {
                        curve: 'smooth'
                    },
                    title: {
                        text: 'Positive/Negative',
                        align: 'left'
                    },
                    grid: {
                        borderColor: '#e7e7e7',
                        row: {
                            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                            opacity: 0.5
                        },
                    },
                    markers: {
                        size: 1
                    },
                    xaxis: {
                        categories: [],
                        title: {
                            text: 'time'
                        },
                        labels: {
                            show: 'false'
                        }
                    },
                    yaxis: {
                        title: {
                            text: 'Numbers'
                        },
                        min: 0,
                        max: 1
                    },
                    legend: {
                        position: 'top',
                        horizontalAlign: 'right',
                        floating: true,
                        offsetY: -25,
                        offsetX: -5
                    }
                },


            };
            let link = "/record" + "/" + window.location.href.split("/")[window.location.href.split("/").length - 3] + "/"
                + window.location.href.split("/")[window.location.href.split("/").length - 2]
                + "/" + window.location.href.split("/")[window.location.href.split("/").length - 1];
            return (
                <div className="chart">
                    <Link to={link}><Back/></Link>
                    <Graph_links/>
                    <div className="chart_container">
                        <Chart
                            options={graph.options}
                            series={graph.series}
                            type="line"
                            width="1200"

                        />
                    </div>
                </div>
            );
        }
        else {
            let link = "/record" + "/" + window.location.href.split("/")[window.location.href.split("/").length - 3] + "/"
                + window.location.href.split("/")[window.location.href.split("/").length - 2]
                + "/" + window.location.href.split("/")[window.location.href.split("/").length - 1];
            return (
                <div className="chart">
                    <Link to={link}><Back/></Link>
                    <Graph_links/>
                    <div className="chart_container">
                        loading...
                    </div>
                </div>
            )
        }
    }
}

export default Posneg;