// вовлеченность 1) кол-во положительных/негативным 2) активные к пассивным 3)
//График эмоций со временем
import React, { Component } from "react";
import Chart from "react-apexcharts";
import '../style.scss';
import Graph_links from "../../record/Graph_links";
import {Link} from "react-router-dom";
import Back from "../Back";

class ApChrt extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        let params = this.props.match.params
        let link = "https://facerecog.miem.tv/record/" + params.room + "/" + params.date + "/" + params.time
        fetch(link)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data.frames
                });
            });
    }

    render(){
        function getMaxOfArray(numArray) {
            return Math.max.apply(null, numArray);
        }

        if( this.state.isLoaded){
            let dataJson = {}
            dataJson.frames = this.state.items
            console.log(dataJson.frames)
            let framesCount = dataJson.frames.length;
            let FrameEmotes = [];
            let Emotes = [[],[],[],[],[],[]]
            let cnt = 0
            let maxNum = 0
            let frames_skipped = 25
            FrameEmotes.push([0, 0, 0, 0, 0, 0]);
            for(let i = 0; i < framesCount; i+=1){
                if(i % frames_skipped == 0 && i != 0){
                    Emotes[0].push(FrameEmotes[cnt][0] / frames_skipped)
                    Emotes[1].push(FrameEmotes[cnt][1] / frames_skipped)
                    Emotes[2].push(FrameEmotes[cnt][2] / frames_skipped)
                    Emotes[3].push(FrameEmotes[cnt][3] / frames_skipped)
                    Emotes[4].push(FrameEmotes[cnt][4] / frames_skipped)
                    Emotes[5].push(FrameEmotes[cnt][5] / frames_skipped)
                    FrameEmotes.push([0, 0, 0, 0, 0, 0]);
                    cnt++
                }
                for (let j = 0; j < dataJson.frames[i].length; j++){
                    if (dataJson.frames[i] != undefined)
                        if(dataJson.frames[i][j][1] == "FEAR")
                            FrameEmotes[cnt][0]++;
                        if(dataJson.frames[i][j][1] == "NEUTRAL")
                            FrameEmotes[cnt][1]++;
                        if(dataJson.frames[i][j][1] == "HAPPY")
                            FrameEmotes[cnt][2]++;
                        if(dataJson.frames[i][j][1] == "SAD")
                            FrameEmotes[cnt][3]++;
                        if(dataJson.frames[i][j][1] == "ANGRY")
                            FrameEmotes[cnt][4]++;
                        if(dataJson.frames[i][j][1] == "SURPRISE")
                            FrameEmotes[cnt][5]++;
                }

            }
            console.log(getMaxOfArray(Emotes[0]))
            if(maxNum < getMaxOfArray([getMaxOfArray(Emotes[0]), getMaxOfArray(Emotes[1]), getMaxOfArray(Emotes[2]), getMaxOfArray(Emotes[3]), getMaxOfArray(Emotes[4]), getMaxOfArray(Emotes[5])]))
                maxNum = getMaxOfArray([getMaxOfArray(Emotes[0]), getMaxOfArray(Emotes[1]), getMaxOfArray(Emotes[2]), getMaxOfArray(Emotes[3]), getMaxOfArray(Emotes[4]), getMaxOfArray(Emotes[5])]);
            let graph = {

                series: [
                    {
                        name: "Fear",
                        data: Emotes[0]
                    },
                    {
                        name: "Neutral",
                        data: Emotes[1]
                    },
                    {
                        name: "Happy",
                        data: Emotes[2]
                    },
                    {
                        name: "Sad",
                        data: Emotes[3]
                    },
                    {
                        name: "Angry",
                        data: Emotes[4]
                    },
                    {
                        name:"Surprise",
                        data: Emotes[5]
                    }
                ],
                options: {
                    chart: {
                        height: 300,
                        type: 'line',
                        dropShadow: {
                            enabled: true,
                            color: '#000',
                            top: 18,
                            left: 7,
                            blur: 10,
                            opacity: 0.2
                        },
                        toolbar: {
                            autoSelected: 'zoom'
                        }
                    },
                    colors: ['#000000', '#0000FF','#00FF00', '#995555', '#FF0000', '#4499FF'],
                    dataLabels: {
                        enabled: true,
                    },
                    stroke: {
                        curve: 'smooth'
                    },
                    title: {
                        text: 'Эмоции на занятии',
                        align: 'left'
                    },
                    grid: {
                        borderColor: '#e7e7e7',
                        row: {
                            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                            opacity: 0.5
                        },
                    },
                    markers: {
                        size: 1
                    },
                    xaxis: {
                        categories: [],
                        title: {
                            text: 'time'
                        },
                        labels: {
                            show: 'false'
                        }
                    },
                    yaxis: {
                        title: {
                            text: 'Numbers'
                        },
                        min: 0,
                        max: maxNum
                    },
                    legend: {
                        position: 'top',
                        horizontalAlign: 'right',
                        floating: true,
                        offsetY: -25,
                        offsetX: -5
                    }
                },


            };
            let link = "/record" + "/" + window.location.href.split("/")[window.location.href.split("/").length - 3] + "/"
                +  window.location.href.split("/")[window.location.href.split("/").length - 2]
                + "/" + window.location.href.split("/")[window.location.href.split("/").length - 1];
            return (
                <div className="chart">
                    <Link to={link}><Back/></Link>
                    <Graph_links/>
                    <div className="chart_container">
                        <Chart
                            options={graph.options}
                            series={graph.series}
                            type="line"
                            width="1200"

                        />
                    </div>
                </div>
            );
        }
        else{
            let link = "/record" + "/" + window.location.href.split("/")[window.location.href.split("/").length - 3] + "/"
                +  window.location.href.split("/")[window.location.href.split("/").length - 2]
                + "/" + window.location.href.split("/")[window.location.href.split("/").length - 1];
            return (
                <div className="chart">
                    <Link to={link}><Back/></Link>
                    <Graph_links/>
                    <div className="chart_container">
                        loading...
                    </div>
                </div>
            );
        }
    }

}

export default ApChrt;