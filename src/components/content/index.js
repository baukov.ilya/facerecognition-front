import React from 'react';
import './style.css';
import List from "./list";
import ApChrt from "./graphs/overall/graph";
import Posneg from "./graphs/posneg";
import Record from "./record";
import Invol from "./graphs/involvement";
import Aktpos from "./graphs/aktpos";
import Add_record from "./add_record/add_record";
import Add_student from "./add_student/add_student";
import {BrowserRouter, Route} from "react-router-dom";
class Content extends React.Component{
    render() {
        return(
            <div className="content_container">
                <Route  exact component={List} path='/'/>
                <Route  exact component={Record} path='/record/:room/:date/:time'/>
                <Route  component={ApChrt} path='/record/overall/:room/:date/:time'/>
                <Route  component={Posneg} path='/record/negpos/:room/:date/:time'/>
                <Route  component={Invol} path='/record/involvement/:room/:date/:time'/>
                <Route  component={Aktpos} path='/record/aktpos/:room/:date/:time'/>
                <Route  component={Add_record} path='/add_record'/>
                <Route  component={Add_student} path='/add_student'/>

            </div>
        )
    }

}



export default Content;