import React from 'react';
import './style.css';
import List_item from "./list_Item";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';

class Record_list extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("https://facerecog.miem.tv/record")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data
                });
            });

    }



    render() {
        if(this.state.isLoaded)
            return(
                <div className="list">
                    <div className="list_header">Список конфигов</div>
                    <div className="list_container">
                        {this.RenderList()}
                    </div>
                </div>
            )
        else
            return (
                <div>loading...</div>
            )

    }

    RenderList(){
        const { error, isLoaded, items} = this.state;
        if( isLoaded) {
            let data = []
            for(let i = 0; i < Object.keys(items.file).length; i++){
                data[i] = [items.room_num[i], items.rec_time[i], items.rec_date[i]]
            }
            return (
                data.map((item) => {
                    return (
                        <List_item data={item}/>
                    )
                })
            )
        }
        else{
            return (
                <div> Records not founded </div>
            )
        }
    }
}



export default Record_list;