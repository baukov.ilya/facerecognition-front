import React from 'react';
import './style.css';

class Add_record extends React.Component{
    render() {
        return(
            <div className="add_record">
                <form action="">
                    <div className="inputs_container">
                        <div className="small_inputs_container">
                            <div className="input_container">
                                <label>confidence_threshold</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>top_k</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>nms_threshold</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>keep_top_k</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>vis_thres</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>network</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>distance_threshold</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>samples</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>eps</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                            <div className="input_container">
                                <label>fps_factor</label>
                                <input className="small_input"  type="text" placeholder="..."/>
                            </div>
                        </div>
                    </div>
                    <div className="config__load-button" onClick={this.handleLoadButton}>Загрузить</div>
                </form>
            </div>

        )
    }
    handleLoadButton(){
        console.log("1")
        let inputs = document.getElementsByClassName( "small_input")
        let link = "/"
        if(inputs[0] != undefined && inputs[0].value != null){
            if (inputs[0].value != "" && inputs[1].value != "" && inputs[2].value != "" && inputs[3].value != "" && inputs[4].value != "" && inputs[5].value != "" && inputs[6].value != "" && inputs[7].value != "" && inputs[8].value != "" && inputs[9].value != "") {
                let linkData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                for (let i = 0; i < 10; i++){
                    if (inputs[i].value.indexOf(".") == -1 && i != 5 && i != 9 && i != 7 && i != 1 && i != 3)
                        linkData[i] = inputs[i].value + ".0"
                    else
                        linkData[i] = inputs[i].value
                }
                console.log(linkData)
                link = 'https://facerecog.miem.tv/config/' + linkData[0] + "/" + linkData[1] + "/" + linkData[2] + "/" + linkData[3] + "/" + linkData[4] + "/" + linkData[5] + "/" + linkData[6] + "/" + linkData[7] + "/" + linkData[8] + "/" + linkData[9];

                fetch(link,{
                    method: 'PUT'
                })
                alert("Данные загружены")

            }
            else
                alert("заполните все поля")
            console.log(link)
        }
    }
}



export default Add_record;