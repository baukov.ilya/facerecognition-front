import React from 'react';
import './style.css';
import {Link} from "react-router-dom";

class Record_info extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        let link = "https://facerecog.miem.tv/record/" + this.props.match.params.room + "/" + this.props.match.params.date + "/" + this.props.match.params.time
        fetch(link)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data
                });
            });

    }
    render() {

        if(this.state.isLoaded && this.state.items != "Video not found")
        return(
            <div className="record_info">
                <div className="record_info">Дата: {this.props.match.params.date}</div>
                <div className="record_info">Время: {this.props.match.params.time}</div>
                <div className="record_info">Аудитория: {this.props.match.params.room}</div>
            </div>



        )
        else
            return (
                <div>no info</div>
            )
    }
}

export default Record_info