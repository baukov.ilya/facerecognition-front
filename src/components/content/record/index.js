import React from 'react';
import './style.css';
import {Link, Route} from 'react-router-dom'
import Graph_links from "./Graph_links";
import Record_info from "./record_info";
import Back from "../graphs/Back";
class Record extends React.Component{
    render() {
        let name = "2020-02-06_09-30_504_26.mp3";
        return(
            <div className="record">
                <Link to="/"><Back/></Link>
                <Route path='/record/:room/:date/:time' component={Graph_links}/>
                <Route exact path='/record/:room/:date/:time' component={Record_info}/>
            </div>


        )
    }

}


export default Record;