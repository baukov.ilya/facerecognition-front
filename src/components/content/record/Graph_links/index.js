import React from 'react';
import './style.css';
import {Link} from "react-router-dom";

class Graph_links extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        let link = "https://facerecog.miem.tv/record/" + window.location.href.split("/")[window.location.href.split("/").length - 3] + "/"
        +  window.location.href.split("/")[window.location.href.split("/").length - 2]
        + "/" + window.location.href.split("/")[window.location.href.split("/").length - 1];
        fetch(link)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data
                });
            });

    }

    render() {
        if(this.state.isLoaded && this.state.items != "Video not found"){


            let link = "/" + window.location.href.split("/")[window.location.href.split("/").length - 3] + "/"
                +  window.location.href.split("/")[window.location.href.split("/").length - 2]
                + "/" + window.location.href.split("/")[window.location.href.split("/").length - 1];
            return (
                <div className="Graph_links_container">
                    <Link to={"/record/overall" + link} className="Graph_links_container_link_text">Эмоции со временем</Link>
                    <Link to={"/record/involvement" + link} className="Graph_links_container_link_text">Вовлеченность</Link>
                    <Link to={"/record/negpos" + link} className="Graph_links_container_link_text">Положительные/Негативные</Link>
                    <Link to={"/record/aktpos" + link} className="Graph_links_container_link_text">Активные/Пассивные</Link>
                </div>
            )
        }
        else
            return (
                <div></div>
            )
    }
}
export default Graph_links
