import React from 'react';
import './style.css';
import List_item from "./list_Item";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';

class List extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            link: ["Все", "Все", "Все"],
            auditories: ["Все"],
            onloadAuditories: false
        };
    }

    componentDidMount() {
        fetch("https://facerecog.miem.tv/record")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data
                });
                let audito = ["Все"]
                for (let i = 0; i < Object.keys(data.file).length; i++){
                    if (!audito.includes(data.room_num[i])){
                        audito.push(data.room_num[i])
                    }
                }

                this.setState({
                    auditories: audito,
                    onloadAuditories: true
                })
            });


    }



    render() {
        if(this.state.isLoaded)
        return(
            <div className="list">
                <div className="list_header">Список записей</div>
                <div className="search_container">
                    <div className="input_container">
                        <label>Номер аудитории</label>
                        <select  className="search_input">
                            <option>Все</option>
                            <option>504</option>
                            <option>307</option>
                        </select>
                    </div>
                    <div className="input_container">
                        <label>Дата</label>
                        <input  type="text" className="search_input" placeholder="..."/>
                    </div>
                    <div className="input_container">
                        <label>Время записи</label>
                        <input  type="text" className="search_input" placeholder="..."/>
                    </div>
                    <Link className="search_link" to="/" onClick={() => this.handleSearchButton()}>
                        <div className="search_btn"><FontAwesomeIcon icon={faSearch} /></div>
                    </Link>
                </div>
                <div className="list_container">
                    {this.RenderList()}
                </div>
            </div>
        )
        else
            return (
                <div>loading...</div>
            )

    }

    RenderAuditories(){

        const {auditories, onloadAuditories} = this.state;
        if(onloadAuditories){
            auditories.map((item) => {
                console.log(item)
                return(
                    <option>{item}</option>
                )
            })
        }
    }

    RenderList(){
        const { error, isLoaded, items, link} = this.state;
        if( isLoaded) {
            let data = []
            let cnt=0
            for(let i = 0; i < Object.keys(items.file).length; i++){
                if(link[0] == "Все" && link[1] == "Все" && link[2] == "Все") {
                    data[cnt] = [items.room_num[i], items.rec_time[i], items.rec_date[i]]
                    cnt++
                }
                else if ((items.room_num[i] == link[0] || link[0] == "Все") && (items.rec_date[i] == link[1] || link[1] == "Все") && (items.rec_time[i] == link[2] || link[2] == "Все")){
                    data[cnt] = [items.room_num[i], items.rec_time[i], items.rec_date[i]]
                    cnt++
                }
            }
            return (
                data.map((item) => {
                    return (
                        <List_item data={item}/>
                    )
                })
            )
        }
        else{
            return (
                <div> Records not founded </div>
            )
        }
    }

    handleSearchButton(){
        // let inputs = document.getElementsByClassName( "search_input")
        // let link = "/"
        // let btn = document.getElementsByClassName("search_link")
        // if(inputs[0] != undefined && btn[0] != undefined && inputs[0].value != null){
        //     link = '/record/' + inputs[0].value + "/" + inputs[1].value + "/" + inputs[2].value;
        //     console.log(link)
        //     this.props.history.push(link)
        // }
        console.log(this)

        let inputs = document.getElementsByClassName( "search_input")
        let btn = document.getElementsByClassName("search_link")
        let link = [0, 0, 0]
        fetch("https://facerecog.miem.tv/record")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    isLoaded: true,
                    items: data
                });
            });
        if(inputs[0] != undefined && btn[0] != undefined && inputs[2] != undefined) {
            if(inputs[0].value != "Все" && inputs[0].value != "")
                link[0] = inputs[0].value
            else
                link[0] = "Все"
            if(inputs[1].value != null && inputs[1].value != "")
                link[1] = inputs[1].value
            else
                link[1] = "Все"
            if (inputs[2].value != null && inputs[2].value != "")
                link[2] = inputs[2].value
            else
                link[2] = "Все"

            this.setState({
                link: link
            })


        }
    }
}


export default List;