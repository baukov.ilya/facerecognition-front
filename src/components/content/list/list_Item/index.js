import React from 'react';
import './style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom';

class List_item extends React.Component {
    render() {
        const {data} = this.props;
        let link_to = "/record/" + data[0] + "/" + data[2] + "/" + data[1];
        return (
            <Link className="item" to={link_to}>
                <div className="date">{data[2]}</div>
                <div className="time">{data[1]}</div>
                <div className="auditory">{data[0]}</div>
                <div className="buttons">
                    <div className="show_btn"><FontAwesomeIcon icon={faSearch}/></div>
                </div>
            </Link>

        )
    }

}



export default List_item;