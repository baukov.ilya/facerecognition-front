import React from 'react';
import './style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

class Add_student extends React.Component{
    render() {
        return(
            <div className="add_student">
                <form action="">
                    <div className="inputs_container">
                        <div className="input_container">
                            <label>Фотография студента</label>
                            <input type="file"  className="nobord" placeholder=""/>
                        </div>
                        <div className="input_container">
                            <label>Имя студента</label>
                            <input type="text" className="student_input" placeholder="Имя"/>
                        </div>
                        <div className="input_container">
                            <label>Фамилия студента</label>
                            <input type="text" className="student_input" placeholder="Фамилия"/>
                        </div>
                        <div className="input_container">
                            <label>Отчество студента</label>
                            <input type="text" className="student_input" placeholder="Отчество"/>
                        </div>
                        <div className="input_container">
                            <label>Номер группы студента</label>
                            <input type="text" className="student_input" placeholder="Группа"/>
                        </div>
                    </div>
                    <div className="config__load-button" onClick={this.handleLoadButton}>Загрузить</div>
                </form>
            </div>

        )
    }
    handleLoadButton(){
        let inputs = document.getElementsByClassName( "student_input")
        let link = "/"
        if(inputs[0] != undefined && inputs[0].value != null){
            if (inputs[0].value != "" && document.getElementsByClassName("nobord")[0].files.length != 0) {
                link = 'https://facerecog.miem.tv/face/' + inputs[0].value;
                let data = new FormData()
                data.append('file',document.getElementsByClassName("nobord")[0].files[0]);
                fetch(link,{
                    method: 'POST',
                    body:data
                });
                alert("Данные загружены")
            }
            else
                alert("заполните все поля")
            console.log(link)
        }
    }
}


export default Add_student;