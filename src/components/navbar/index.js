import React from 'react';
import './style.css';
import {GoogleLogout} from 'react-google-login';

class Navbar extends React.Component{

    logout = () =>{
        sessionStorage.setItem("logined", "false");
        sessionStorage.setItem("mail", " ");
        localStorage.setItem("access_token", " ")
        window.location.reload();

}
    render() {
        console.log(this.props);
        return(
            <header className="nav">
                <div className="nav_name">Распознование медиаконтента</div>
                <div className="nav_info">
                    <div className="nav_info_text">{sessionStorage.mail}</div>
                    <GoogleLogout
                        clientId="332884163839-cgsk3ta79lgoo2o2otcb2h8ck28cd1if.apps.googleusercontent.com"
                        buttonText="Logout"
                        onLogoutSuccess={this.logout}
                    >
                    </GoogleLogout>

                </div>
            </header>
        )
    }
    handleExitBtn(){
        sessionStorage.setItem("logined", "false");
        sessionStorage.setItem("name", " ");
        sessionStorage.setItem("surName", " ");
        localStorage.setItem("access_token", " ")
        window.location.reload();
    }

}



export default Navbar;