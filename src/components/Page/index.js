import React from 'react';
import './style.css';
import Navbar from "../navbar";
import Sidebar from "../sidebar";
import Content from '../content'
import {Route} from "react-router-dom"

class Page extends React.Component{

    render() {
        return(
            <div className="Page">
                <Navbar state ={this.props.state}/>
                <main className="container">
                   <Route component={Sidebar} path="/"/>
                   <Content state = {this.state}/>
                </main>
            </div>
        )
    }


}



export default Page;