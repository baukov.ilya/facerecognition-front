import React, { Component } from 'react';
import GoogleLogin from "react-google-login";
import Page from "./components/Page";
import './App.css';
import {BrowserRouter} from "react-router-dom";
import { refreshTokenSetup } from './utils/refreshToken';

export class App extends Component{
    constructor(state) {
        super(state);
        this.state = {logined: false,
                      userInfo : {},
                      userName: sessionStorage.getItem("name"),
                      userSurname: sessionStorage.getItem("surName"),
                      };
    }

    responseGoogle = async (response) => {
        console.log(response)
        localStorage.setItem("access_token", response.accessToken)
        sessionStorage.setItem("logined", "true");
        this.checkToken()
        refreshTokenSetup(response);
        await window.location.reload();

    }
    checkToken(){
        if(localStorage.access_token != " "){
            fetch('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' + localStorage.access_token)
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    if (data.issued_to == "332884163839-cgsk3ta79lgoo2o2otcb2h8ck28cd1if.apps.googleusercontent.com" && data.access_type == "online"){
                        sessionStorage.setItem("logined", "true");
                        sessionStorage.setItem("mail", data.email)
                    }
                    else
                        sessionStorage.setItem("logined", "false");

                });
        }
    }
    checkLoop(){
        setInterval(this.checkToken(), 100000)
    }
    responseGoogleFailure = (response) => {
        alert("Ошибка входа");
    }
  render(){
        this.checkToken()
      if(sessionStorage.logined == "true")
          return(
              <BrowserRouter>
                  <Page state={this.state}/>
              </BrowserRouter>

          )
      else return(
          <div className="Google_auth_container">
              <GoogleLogin
                  className="Google_Login_btn"
                  // clientId="729991945562-i4erkc1ljem5n9qr4jn8s7cifvhc7skc.apps.googleusercontent.com"
                  clientId="332884163839-cgsk3ta79lgoo2o2otcb2h8ck28cd1if.apps.googleusercontent.com"
                  buttonText="Login"
                  onSuccess={this.responseGoogle}
                  onFailure={this.responseGoogleFailure}
                  cookiePolicy={'single_host_origin'}
              />
          </div>
      );
  };
}


export default App;
